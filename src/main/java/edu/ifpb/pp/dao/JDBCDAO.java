/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ifpb.pp.dao;

import edu.ifpb.pp.qualifiers.JDBC;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Default;
import javax.inject.Named;

/**
 *
 * @author Ricardo Job
 */
@Named("jdbc")
@RequestScoped
@Default
@JDBC
public class JDBCDAO implements DAO {

    @Override
    public String salvar() {
        return "JDBC";
    }

}
