/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ifpb.pp.dao;

import edu.ifpb.pp.qualifiers.XML;
import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Alternative;
import javax.inject.Named;

/**
 *
 * @author Ricardo Job
 */
@Named("xml")
@RequestScoped
@Alternative
@XML
public class XMLDAO implements DAO, Serializable {

    //@Override
    public String salvar() {
        return "XML";
    }

}
