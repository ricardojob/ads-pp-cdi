package edu.ifpb.pp;

import java.io.Serializable;

/**
 *
 * @author Ricardo Job
 */
public class Pessoa implements Serializable {

    private String nome;
    private double id;

    public Pessoa(String nome, double id) {
        this.nome = nome;
        this.id = id;
    }

    public Pessoa() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getId() {
        return id;
    }

    public void setId(double id) {
        this.id = id;
    }

}
