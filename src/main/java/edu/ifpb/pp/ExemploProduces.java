package edu.ifpb.pp;

import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Produces;
import javax.inject.Named;

/**
 *
 * @author Ricardo Job
 */
@Named
@Dependent
public class ExemploProduces {

    @Named
    @Produces
    private String nomePessoa = "Chaves";

    @Named("pessoaNova")
    @Produces
    public Pessoa getPessoa() {
        double id = Math.random() * 10;
        return new Pessoa("Kiko", id);
    }

    @Named
    @Produces
    public List<Pessoa> listaPessoas() {

        List<Pessoa> lista = new ArrayList<>();
        lista.add(new Pessoa("Nhonho", Math.random() * 10));
        lista.add(new Pessoa("Godiles", Math.random() * 10));
        lista.add(new Pessoa("Girafales", Math.random() * 10));
        return lista;
    }
}
