package edu.ifpb.pp.jsf;

import edu.ifpb.pp.Pessoa;
import edu.ifpb.pp.dao.DAO;
import edu.ifpb.pp.qualifiers.JDBC;
import edu.ifpb.pp.qualifiers.JPA;
import edu.ifpb.pp.qualifiers.XML;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Ricardo Job
 */
@Named("controlador")
@RequestScoped
public class Controlador {

    @Inject
    private Pessoa pessoa;

//       @Inject
//    private List<Pessoa> listaDePessoa;
    @Inject
    @JPA
    private DAO jpa;

    @Inject
    @JDBC
    private DAO jdbc;

    @Inject
    @XML
    private DAO xml;

    public Pessoa getPessoa() {
        return pessoa;
    }

    public DAO getJpa() {
        return jpa;
    }

    public DAO getJdbc() {
        return jdbc;
    }

    public DAO getXml() {
        return xml;
    }

}
